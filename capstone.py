from abc import ABC

class Person(ABC):

    def getFullName():
        pass
    def addRequest():
        pass
    def checkRequest():
        pass
    def addUser():
        pass


class Employee(Person):
    def __init__(self, firstName, lastName, email, department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    def getFullName(self):
        fullName = self._firstName + " " + self._lastName
        return fullName 

    def get_firstName(self):
        print(self._firstName)
    def set_firstName(self, firstName):
        self._firstName = firstName

    def get_lastName(self):
        print(self._lastName)
    def set_lastName(self, lastName):
        self._lastName = lastName

    def get_email(self):
        print(self._email)
    def set_email(self, email):
        self._email = email
    
    def get_department(self):
        print(self._department)
    def set_department(self, department):
        self._department = department
    

    def checkRequest(self):
        print("This is a placeholder")
    def addUser(self):
        print("This is a placeholder")
    def login(self):
        return f"{self._email} has logged in"
    def logout(self):
        return f"{self._email} has logged out"

    def addRequest(self):
        return "Request has been added"

class TeamLead(Person):
    def __init__(self, firstName, lastName, email, department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department
        self._members = []

    def getFullName(self):
        fullName = self._firstName + " " + self._lastName
        return fullName

    def get_firstName(self):
        print(self._firstName)
    def set_firstName(self, firstName):
        self._firstName = firstName

    def get_lastName(self):
        print(self._lastName)
    def set_lastName(self, lastName):
        self._lastName = lastName

    def get_email(self):
        print(self._email)
    def set_email(self, email):
        self._email = email
    
    def get_department(self):
        print(self.__department)
    def set_department(self, department):
        self._department = department
    

    def checkRequest(self):
        print("This is a placeholder")
    def addUser(self):
        print("This is a placeholder")
    def login(self):
        return f"{self._email} has logged in"
    def logout(self):
        return f"{self._email} has logged out"
    
    def addMember(self, employee):
        self._members.append(employee)
    
    def get_members(self):
        return self._members
    

class Admin(Person):
    def __init__(self, firstName, lastName, email, department):
        self._firstName = firstName
        self._lastName = lastName
        self._email = email
        self._department = department

    def getFullName(self):
        fullName = self._firstName + " " + self._lastName
        return fullName

    def get_firstName(self):
        print(self._firstName)
    def set_firstName(self, firstName):
        self._firstName = firstName

    def get_lastName(self):
        print(self._lastName)
    def set_lastName(self, lastName):
        self._lastName = lastName

    def get_email(self):
        print(self._email)
    def set_email(self, email):
        self._email = email
    
    def get_department(self):
        print(self._department)
    def set_department(self, department):
        self._department = department
    

    def checkRequest(self):
        print("This is a placeholder")
    def addUser(self):
        return "User has been added"
    def login(self):
        return f"{self._email} has logged in"
    def logout(self):
        return f"{self._email} has logged out"


class Request():
    def __init__(self, name, requester, dataRequested):
        self.name = name
        self.requester = requester
        self.dataRequested = dataRequested
        self.status = "requested"
    
    def updateRequest(self):
        pass
    def closeRequest(self):
        return "Request Laptop repair has been closed"
    def cancelRequest(self):
        pass
    def set_status(self, status):
        self.status = status





# Test cases:
employee1 = Employee("John", "Doe", "djohn@mail.com", "Marketing")
employee2 = Employee("Jane", "Smith", "sjane@mail.com", "Marketing")
employee3 = Employee("Robert", "Patterson", "probert@mail.com", "Sales")
employee4 = Employee("Brandon", "Smith", "sbrandon@mail.com", "Sales")
admin1 = Admin("Monika", "Justin", "jmonika@mail.com", "Marketing")
teamLead1 = TeamLead("Michael", "Specter", "smichael@mail.com", "Sales")
req1 = Request("New hire orientation", teamLead1, "27-Jul-2021")
req2 = Request("Laptop repair", employee1, "1-Jul-2021")

assert employee1.getFullName() == "John Doe", "Full name should be John Doe"
assert admin1.getFullName() == "Monika Justin", "Full name should be Monika Justin"
assert teamLead1.getFullName() == "Michael Specter", "Full name should be Michael Specter"
assert employee2.login() == "sjane@mail.com has logged in"
assert employee2.addRequest() == "Request has been added"
assert employee2.logout() == "sjane@mail.com has logged out"

teamLead1.addMember(employee3)
teamLead1.addMember(employee4)

for indiv_emp in teamLead1.get_members():
     print(indiv_emp.getFullName())

assert admin1.addUser() == "User has been added"

req2.set_status("closed")
print(req2.closeRequest())


